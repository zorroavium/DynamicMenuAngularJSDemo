﻿/// <reference path="MenuScript.js" />
app.controller('registrationController', function ($scope, $http) {
    $scope.newUser = {};
    $scope.clickedUser = {};
    $scope.masterUserData = [];
    $scope.message = "";

    $http.get('../DynamicMenuAngularJS/Home/GetAllUser')
           .then(function (d) {         
               for (var i = 0; i < d.data.length; i++) {
                   d.data[i].DateOfBirth = new Date(parseInt((d.data[i].DateOfBirth).substr(6)));                            
               }                             
               $scope.Data = d.data;
               angular.copy($scope.Data, $scope.masterUserData);
           }, function (error) {
               alert(error);
           })
    //Dropdown for Department
    $scope.options = ['HR', 'IT', 'Finance', 'others'];
    $scope.newUser.Department = $scope.options;

    //Sorting data by header elements
    $scope.sortColumn = "Id";
    $scope.reverseSort = false;

    $scope.sortData = function (column) {
        $scope.reverseSort = ($scope.sortColumn == column) ?
            !$scope.reverseSort : false;
        $scope.sortColumn = column;
    }
    $scope.getSortClass = function (column) {
        if ($scope.sortColumn == column) {
            return $scope.reverseSort
              ? 'arrow-down'
              : 'arrow-up';
        }
        return '';
    }

    //Add a new user
    $scope.create = function (data) {
        ////DB update
        $http.post('/DynamicMenuAngularJS/Home/CreateUser', data)
              .then(function (id) {
                  $scope.Data.push({
                      "Id": id.data,
                      "UserName": data.UserName,
                      "Password": data.Password,
                      "FirstName": data.FirstName,
                      "LastName": data.LastName,
                      "Gender": data.Gender,
                      "DateOfBirth": data.DateOfBirth,
                      "Email": data.Email,
                      "Department": data.Department,
                      "Address": data.Address
                  });
                  $scope.Data.Id = id.data;
                  //updating user in html table
                  angular.copy($scope.Data, $scope.masterUserData);                 
              }),
        function (error) {
            alert(error);
        }      
        $scope.message = "New User Registered Successfully!";
    }

    //Clear add user form everytime before open
    $scope.clearForm = function () {
        $scope.userForm.$setPristine();
    }

    // select user data on click of edit/delete button
    $scope.selectUser = function (data) {
        $scope.clickedUser = data;
    }

    //restore data on cancel/close
    $scope.restore = function () {
        $scope.Data = angular.copy($scope.masterUserData);
        $scope.newUser = {};
    }

    //Delete user record when yes is pressed
    $scope.delete = function () {
        //DB update
        $http.post('/DynamicMenuAngularJS/Home/DeleteUser', $scope.clickedUser)
              .then(function (deletedUser) {                 
                  for (var i = 0; i < $scope.Data.length; i++) {
                      if ($scope.Data[i].Id === $scope.clickedUser.Id) {
                          index = i;
                          break;
                      }
                  }
                  //removing user in html table
                  $scope.Data.splice(index, 1);
                      $scope.message = "User "+deletedUser.data+ " Deleted Successfully!";               
              }),
        function (error) {
            alert(error);
        }
        
    }

    //update user Record
    $scope.update = function (clickedUser) {
        //DB update
        $http.post('/DynamicMenuAngularJS/Home/UpdateUser', clickedUser)
              .then(function (updatedUser) {
                  //updating user in html table
                  angular.copy($scope.Data, $scope.masterUserData);
                  $scope.message = "User " + updatedUser.data + " Updated Successfully!";
              }),
        function (error) {
            alert(error);
        }
      
    }
    $scope.clearMessage = function () {
        $scope.message = "";
    }
});




   
