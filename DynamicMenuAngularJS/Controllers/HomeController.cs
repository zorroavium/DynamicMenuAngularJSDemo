﻿using DynamicMenuAngularJS.Models;
using DynamicMenuAngularJS.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicMenuAngularJS.Controllers
{

    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Temp()
        {
            return View();
        }
        //Table Data
        public ActionResult GetAllUser()
        {
            using (RegistrationContext db = new RegistrationContext())
            {
                var JoinData = db.Registrations.Join(db.Logins, registrationTableUser => registrationTableUser.Id, loginTableUser => loginTableUser.Id, (registrationTableUser, loginTableUser) =>
                    new UserDetails
                    {
                        UserName = loginTableUser.UserName,
                        Password = loginTableUser.Password,
                        Id = registrationTableUser.Id,
                        FirstName = registrationTableUser.FirstName,
                        LastName = registrationTableUser.LastName,
                        Gender = registrationTableUser.Gender,
                        DateOfBirth = registrationTableUser.DateOfBirth,
                        Email = registrationTableUser.Email,
                        Department = registrationTableUser.Department,
                        Address = registrationTableUser.Address,

                    }).ToList();
                return new JsonResult { Data = JoinData, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        public int CreateUser(CreateModel createModel)
        {
            using (RegistrationContext db = new RegistrationContext())
            {
                Registration registration = new Registration();
                Login login = new Login();
                if (ModelState.IsValid)
                {
                    registration.FirstName = createModel.FirstName;
                    registration.LastName = createModel.LastName;
                    registration.Gender = createModel.Gender;
                    registration.Email = createModel.Email;
                    registration.DateOfBirth = createModel.DateOfBirth;
                    registration.Department = createModel.Department;
                    registration.Address = createModel.Address;
                    db.Registrations.Add(registration);
                    login.Id = registration.Id;
                    login.UserName = createModel.UserName;
                    login.Password = createModel.Password;
                    db.Logins.Add(login);
                    db.SaveChanges();
                    return registration.Id;

                }
                return registration.Id;;
            }
        }
      
        public ActionResult EditUser()
        {
            using (RegistrationContext db = new RegistrationContext())
            {
                 var JoinData = db.Registrations.Join(db.Logins, registrationTableUser => registrationTableUser.Id, loginTableUser => loginTableUser.Id, (registrationTableUser, loginTableUser) =>
                    new EditModel
                    {
                        UserName = loginTableUser.UserName,
                        Password = loginTableUser.Password,
                        FirstName = registrationTableUser.FirstName,
                        LastName = registrationTableUser.LastName,
                        Gender = registrationTableUser.Gender,
                        DateOfBirth = registrationTableUser.DateOfBirth,
                        Email = registrationTableUser.Email,
                        Department = registrationTableUser.Department,
                        Address = registrationTableUser.Address
                   });
                return View();
            }
        }
    
        public string UpdateUser(UserDetails editModel)
        {
           using (RegistrationContext db = new RegistrationContext())
            {
                var registration = db.Registrations.Find(editModel.Id);
                var login = db.Logins.Where(loginTableUser => loginTableUser.Id == editModel.Id).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    registration.FirstName = editModel.FirstName;
                    registration.LastName = editModel.LastName;
                    registration.Gender = editModel.Gender;
                    registration.Email = editModel.Email;
                    registration.DateOfBirth = editModel.DateOfBirth;
                    registration.Department = editModel.Department;
                    registration.Address = editModel.Address;
                    db.Entry(registration).State = EntityState.Modified;
                    login.UserName = editModel.UserName;
                    login.Password = editModel.Password;
                    db.Entry(login).State = EntityState.Modified;
                    db.SaveChanges();
                    return login.UserName;
                }

                return login.UserName;
            }
        }
       
        public string DeleteUser(UserDetails deleteModel)
        {
            using (RegistrationContext db = new RegistrationContext())
            {
                var registration = db.Registrations.Find(deleteModel.Id);
                var login = db.Logins.Where(loginTableUser => loginTableUser.Id == deleteModel.Id).FirstOrDefault();
                db.Logins.Remove(login);
                db.Registrations.Remove(registration);
                db.SaveChanges();
                return deleteModel.UserName;
            }
        }

        // MuliLevel Drop down Menu
        public JsonResult GetSiteMenu()
        {
            using (MenuContext db = new MenuContext())
            {
                var menu = db.Menus.ToList();
                var sitemenu = menu.Select(allUser => new
                {
                    Id = allUser.Id,
                    Name = allUser.Name,
                    Pid = allUser.Pid
                });
                return new JsonResult
                {
                    Data = sitemenu,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
}
